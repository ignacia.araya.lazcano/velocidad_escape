#solicitud de variables
from cmath import sqrt


radio = float(input("Ingrese radio del planeta en metros: "))
constante_gravedad = float(input("Ingrese constante gravitacional ms**-2: "))
velocidad_escape = sqrt(2 * radio * constante_gravedad)

print ("La velocidad de escape es", velocidad_escape)